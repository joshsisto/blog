# What I'm Up To Now

Currently working from home in Fair Oaks with my 2 dogs

📚 Entangled Life by Merlin Sheldrake

💻 Studying for my CompTIA Pentest+ Certification

🤹 [Juggling](https://youtu.be/pFj0x3RVyC4) every day!

(Now Page inspired by [Derek Sivers](https://sivers.org/now))

Click [posts](https://sisto.blog/post/) below to see blog posts

[Click here to join my private mail list](https://mailchi.mp/cd24cb2ec3d1/joshsisto)
