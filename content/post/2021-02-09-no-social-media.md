---
title: Deactivating Social Media Accounts
# subtitle: Making a Gallery
date: 2021-02-09
tags: ["social media", "security", "hacking"]
---

I deactivated all of my social media accounts. I did it because I feel like I wasn't really getting much out of it. It is a great tool for staying in touch with certain people but I hardly use it for that. Then there are the privacy concerns. The more I've gotten involved in the hacking community the more I realized I'm creating a larger attack surface for myself. Going to be using this blog as a little bit more of a platform to share what I am up to.

![throwing a disc](https://media.giphy.com/media/xRg1MyCTQvA6rO0LGe/giphy.gif)

Top of the World - DeLaveaga - Feb 6th 2021
