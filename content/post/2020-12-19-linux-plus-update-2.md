---
title: Linux+ Update 2
# subtitle: Making a Gallery
date: 2020-12-19
tags: ["comptia", "linux"]
---

Wanted to share a few resources I discovered while studying. The first is some files and directories provided by [Linux Pocket Guide](https://linuxpocketguide.com/). When you are first playing around with Linux and performing text manipulation and file comparisons you need directories and files to work with. The Linux Pocket Guide provides these resources. I recommend you buy the book, but if you don't the files are publicly hosted. The two commands below will download and extract the files.

`wget http://linuxpocketguide.com/LPG-stuff.tar.gz`
`tar -xf LPG-stuff.tar.gz`

The other cool thing I discovered was a linux password auditing command. This command will display registered users that have access to the system.

`grep -v nologin$ /etc/passwd`
