---
title: Private Internet Access
# subtitle: Making a Gallery
date: 2022-01-19
tags: ["VPN", "networking", "affiliate"]
---

I have been using [Private Internet Access (PIA)](https://www.privateinternetaccess.com/) for over 3 years now. Recently I wrote a [bash script](https://gitlab.com/joshsisto/linux_scripts/-/blob/90d7c185b39cec9ddd928ee3e466dcf284cdf7c4/PIA-VPN.sh) to automate connecting my ubuntu server to PIA via OpenVPN. In the past I had to authenticate with my PIA username and password each time I wanted to connect to a VPN. So I did what any good coder would do and automated this. To get your OpenVPN file login to PIA and go to [OpenVPN Configuration Generator](https://www.privateinternetaccess.com/account/ovpn-config-generator). Select your Platform (in my case Linux), Region, and Port, then select "Generate". This will download a .ovpn file that that I will use to demonstrate connecting with a bash script. In this example the file is named ca-aes-256-gcm-udp-dns.ovpn. Create file named connect-pia-vpn.sh and add the following content.

```bash
#!/bin/bash

/usr/bin/expect <(cat << EOF
spawn sudo openvpn --config ca-aes-256-gcm-udp-dns.ovpn
expect "Enter Auth Username:"
send "PIAUSERNAME\r"
expect "Enter Auth Password: (press TAB for no echo)"
send "PIAPASSWORD\r"
interact
EOF
)
```

Make sure to replace PIAUSERNAME and PIAPASSWORD with your PIA username and password. Use `chmod +x connect-pia-vpn.sh` to make the file executable and then run it using `./connect-pia-vpn.sh`. You should now be connected to your PIA VPN!

[Youtube Link](https://youtu.be/1TtXAvhRg_U)

[Private Internet Access Affiliate Link $79/3Yr+3Mo](http://www.privateinternetaccess.com/pages/buy-a-vpn/1218buyavpn?invite=U2FsdGVkX1-4eBLG-ppPd8IvU42f1CMWjR-y0QwcKgg%2CesfCUAom0620q8G4I4U6XU2zUqI)
