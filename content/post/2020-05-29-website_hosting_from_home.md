---
title: How I Host My Own Websites And Apps From Home For Free
# subtitle: Making a Gallery
date: 2020-05-29
tags: ["hosting", "web dev"]
---

<!-- ## How I Host My Own Websites And Apps From Home For Free -->

I currently host my [Personal Portfolio](https://joshsisto.com), a [Network Scanning App](https://scanner.sisto.solutions), a [Weather App](https://pi.sisto.solutions) and many more sites and services for free using Docker, Nginx, and CloudFlare. The setup is pretty straightforward and has been working without issue for almost a year now. I host most apps using VM's and raspberry pi's that use Nginx as a reverse proxy that connects to cloudflare to protect my home IP.

I run the [Letsencrypt container](https://docs.linuxserver.io/images/docker-letsencrypt) from linuxserver.io on a ubuntu VM on my home server. Here is the [docker compose](https://gitlab.com/joshsisto/docker-compose/-/blob/master/letsencrypt/docker-compose.yaml) file that I use and the [default file](https://gitlab.com/joshsisto/docker-compose/-/blob/master/letsencrypt/default) (located at /root/docker/letsencrypt/nginx/site-confs) that contains the server IP and port used in the reverse proxy setup. For this domain I happen to use google, I've attached an image of the DNS settings [here](https://gitlab.com/joshsisto/docker-compose/-/blob/master/letsencrypt/google_DNS.png). Essentially you need to create an A record and point it to your home address and a CNAME for each subdomain. Run `docker-compose up -d` to start the container. If everything works you should be able to visit the URL of your website, for me it would be [https://sisto.solutions](https://sisto.solutions). You can check the logs using `docker logs letsencrypt/`.

You now have an encrypted website running using letsencrypt, the only problem now is that all websites are resolving to your home IP address. Bad actors could now DDOS you or attempt to hack your home network. You can use cloudflares free service to one domain to solve this issue. The process is simple, setup an account with cloudflare, switch your nameservers over, and forward DNS records the same as you did in google as cloudflare.

You now can create infinite subdomains for apps and sites that you want to host with cloudflare protection.
