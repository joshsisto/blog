---
title: The 7 Habits of Highly Effective People
# subtitle: Making a Gallery
date: 2020-07-15
tags: ["books", "self-help", "motivation", "lifestyle"]
---

Finally finished reading "The 7 Habits of Highly Effective People" the other day and I feel extremely inspired. It has been a very impactful book and to me is a manual on how to live a happy, productive, meaningful life. This book was recommended to me by a good friend years back and I have attempted to read it a few times, I finally saw the book to completion. __Disclaimer:__ I listened to the audiobook version

I'm not going to write out the 7 habits or talk about each one individually, you can read the book yourself if you want. What I want to do is share the inspiration that I pulled from the book. Habit 3 *Putting First Things First* helped me to create my [mission statement](https://gitlab.com/joshsisto/personal-mission-statement/-/blob/master/README.md) which is mentioned extensively throughout the book. A personal mission statement helps guide you in every decision in your life, it is a personal true north. Putting first things first also made me realize the importance of prioritizing all of the things I want to accomplish in a day. One of the most important things you can do as a person is take good care of yourself. That means getting proper rest, exercise, and social time. *Putting First Things First* means you need to schedule these items, preferably in the morning before less important distractions get in the way.

The 7 Habits talks in depth about *paradigms, the most important being "Win-Win". Win-Win is a simple paradigm to understand, in business, and relationships. It is a mutually beneficially deal where both parties win. Not groundbreaking material, but think about how many times you have been in an argument with someone and chose win-lose, or even worse lose-lose. Try and think win-win.

I have been working on my listening skills for many years and will always actively work on improving my listening skills. Habit 5 is *Seek First to understand, Then to Be Understood*. This was very powerful to me, I have always been a good listener and would even say good at empathizing with people. But to truly make sure that you understand where someone is coming from is completely different and truly powerful.

[My Mission Statement](https://gitlab.com/joshsisto/personal-mission-statement/-/blob/master/README.md)

[Books Links](https://sisto.blog/page/books/) Work In Progress
