---
title: Security Hardening & Email Encryption
# subtitle: Making a Gallery
date: 2021-02-20
tags: ["security", "DNS", "pi", "macos", "email", "PGP"]
---

As I go further down the security rabbit hole I'm now in the process of hardening security on all of my devices. I've moved all of my lab behind a second firewall with strict security settings to keep it segregated from my local LAN. I also completely reinstalled several systems to ensure that they haven't been compromised in any way. I've also removed windows from my local network, I now only use windows in virtual machines. For DNS security I'm now using [Pi-hole](https://pi-hole.net/) and enabled DNSSEC. I'm also now using OpenDNS and Quad9 as upstream DNS servers and removed google to use privacy respecting services. Additionally I added a few more blocklists from [Firebog](https://firebog.net/) for increased filtering.

Since I use a Macbook Pro as my daily driver I figured I should lock it down as well. First I reinstalled my MBP to factory to ensure my system hasn't been compromised. I then installed several tools from [Objective-See](https://objective-see.com/products.html). I highly recommend you check them out and [support them](https://www.patreon.com/bePatron?u=4857001) if you use macOS. I also used [drduh](https://github.com/drduh)'s [macOS-Security-and-Privacy-Guide](https://github.com/drduh/macOS-Security-and-Privacy-Guide).

I've also started to use PGP encryption for my emails. If you would like to encrypt a message you can use my public keys below.

josh@joshsisto.com [294CDF7C8D18ABDD5B8A0DAA8CDB89F830BC60EA](https://keys.openpgp.org/search?q=josh@joshsisto.com)

joshsisto@gmail.com [25657AEE55532B8D76D6A68BD42E46B789A16168](https://keys.openpgp.org/search?q=joshsisto@gmail.com)

to easily encrypt email signup for an account with [ProtonMail](https://protonmail.com/) and email me at joshsisto@protonmail.com