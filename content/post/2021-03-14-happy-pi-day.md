---
title: Happy Pi Day
# subtitle: Making a Gallery
date: 2021-03-14
tags: ["pi", "linux"]
---

I changed my Linux+ test date to May 4th. I was stressing on taking it this upcoming Tuesday since I haven't properly prepared for it. I also just freshly installed Big Sur on my 2017 MBP so I'm having to do a lot of this from scratch so this is a good time to start blogging about it. This will be more so notes for myself than an actual blog post.

I will be using raspberry pi's and virtualbox to run various distributions. I have pi's running fedora, openSUSE, ubuntu, and of course Raspbian. 

I will connect to the pi's via SSH. I do so my running the command below

`ssh pi@10.1.1.x`

This connects me to the pi but requires a password each time. I run the command below to install my SSH key on the server for access without a password. Read this for more [https://www.ssh.com/ssh/copy-id](https://www.ssh.com/ssh/copy-id)

`ssh-copy-id pi@10.1.1.x`

Since I had a fresh install on my macbook I needed to run the command below first to generate a SSH Key. [https://www.ssh.com/ssh/keygen/](https://www.ssh.com/ssh/keygen/)

`ssh-keygen`

This creates a public and private key in a hidden directory of the user directory `~/.ssh/`. This folder contains your public and private key as well as a `known_hosts` file. The known_hosts file contains IP's and public keys when you say "yes" to a new SSH connection. This is useful to know because when you connect to a device with a newly created OS and hence new public keys you will have issues connecting to it via SSH because of your known_hosts file. You can fix this by removing the line of your old connection in the known_hosts file.

Once you are connected to your device use the commands below to update on the respective OS.

### Debian

`sudo apt update && sudo apt upgrade -y`

__The command above is saying if "sudo apt update" completes without errors then execute "sudo apt upgrade -y". I like to use -y to automatically accept the package updates__

### RHEL

`sudo yum update`

`sudo dnf upgrade`

### openSUSE

`sudo zypper refresh`

`sudo zypper update`

Since we are learning let's install [neovim](https://neovim.io)

`sudo apt install neovim`

`sudo yum install neovim`

`sudo dnf install neovim`

`sudo zypper install neovim`

If you are using raspbian, ubuntu, or macOS I recommend you try vimtutor. Simply type `vimtutor` into the command line and follow the interactive vim tutorial. If you do not want to learn vim you can use nano in place of the vim command to edit files in most cases.

Once neovim is installed let's alias the vim command to use neovim instead of vim.

Edit `~/.bashrc` to add the alias. Let's use neovim (nvim) to edit the file

`nvim ~/.bashrc`

Add `alias vim="nvim"` to the bottom of the file and save it. Exit your current SSH session and reconnect so that .bashrc is reloaded and you can check the alias by using the `which` command. `which nvim` should show your alias pointing vim to nvim.

![screenfetch](https://gitlab.com/joshsisto/blog/-/raw/master/content/images/screenfetch_x4.png)
