---
title: Josh's Adventures in Wonderland 2020 Wrap Up
# subtitle: Making a Gallery
date: 2020-12-30
tags: ["life", "linux", "journal"]
---

I'm slowly still working towards my linux+ certification. While studying I created a one liner that replaces my name with Alice in the book *Alice's Adventures In Wonderland*.

`curl https://www.gutenberg.org/files/11/11-0.txt | sed 's/Alice/Josh/g' > Joshs-adventures-in-wonderland.txt`

This one liner uses curl to download *Alice's Adventures In Wonderland* from [Project Gutenberg](https://www.gutenberg.org/) and uses sed to replace my name with Alice's name. You can view the output here [Josh's Adventures In Wonderland](https://josh.sisto.solutions/Joshs-adventures-in-wonderland.txt). I hope this gets people thinking about other cool things you can do playing around with publicly available data.

On Monday I started a new job working as a Information Security Analyst for the State of California. I'm excited to start this new role and develop new skills. In response to the change in work I'm focusing much more heavily on security and pentesting. I've subscribed to both [Try Hack Me](https://tryhackme.com/) and [HACKTHEBOX](https://www.hackthebox.eu/). I've completed a few challenges from both platforms. I've also focused heavily on web application security and have been completing [Web Security Challenges](https://portswigger.net/web-security) by [PortSwigger](https://portswigger.net/). Additionally I've installed a local instance of [Nessus](https://www.tenable.com/products/nessus) to scan my local network for vulnerabilities.

2020 has been the year for new hobbies for me. After going to Gem and Jam Music Festival in February of this year I fell in love with juggling. I juggle almost every day, sometimes for 8 hours or more! I've also picked up archery this year. My girlfriend and I have been going to [El Dorado Bowmen's](https://eldoradohillsbowmen.com/) to practice archery. My girlfriend and I have also been getting into woodworking as well. Excited for what the new year has to offer.
