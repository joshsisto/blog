---
title: A Quick Guide To Getting On The Dark Web
# subtitle: Making a Gallery
date: 2020-06-03
tags: ["hosting", "web dev", "dark web"]
---

<!-- ## A Quick Guide To Getting On The Dark Web -->

The Dark Web or Dark Net is an encrypted network with privacy and security in mind. The most popular Dark Net is Tor or The Onion Router. The quickest way to get on the Tor network is to download the [Tor Browser](https://www.torproject.org/download/). You can now browse the internet the same way as before with increased privacy and security, you can also now access .onion addresses. Check out my [Tor Site](http://sw47wfjtafhe22dyc6cyg2leoh52b3tfzxcukjhc7cixzzk64meofpqd.onion/) for additional guides to staying safe on the Dark Web.
### Additional Links

**My Tor Site**

http://sw47wfjtafhe22dyc6cyg2leoh52b3tfzxcukjhc7cixzzk64meofpqd.onion/

**Tor Project**

https://www.torproject.org/
# Warning

This is a quick guide and does not guarantee full privacy and security. Check out [PrivacyTools](https://privacytools.io/) for more information on how to stay safe.
