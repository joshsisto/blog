---
title: NFT
# subtitle: Making a Gallery
date: 2022-01-26
tags: ["NFT", "crypto"]
---

I used advanced data mining techniques to create big data sets for Machine Learning to power AI generated NFT

(Click image to purchase)
[![NFT](https://gitlab.com/joshsisto/blog/-/raw/master/content/images/chameleon_matrix.jpg)](https://opensea.io/assets/matic/0x2953399124f0cbb46d2cbacd8a89cf0599974963/76136580823904046885756552223005819942521272928951773534672051800781203963905/ "opensea")

