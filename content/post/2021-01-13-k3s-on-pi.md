---
title: k3s on raspberry pi
# subtitle: Making a Gallery
date: 2021-01-13
tags: ["linux", "pi", "kubernetes"]
---

Going to share with you how I got kubernetes running on my raspberry pi cluster. For this build I used 1 x raspberry pi 4 8gb model and 3 x raspberry pi 4 4gb models for this build. The case is a C4Labs Zebra Bramble Case. _Warning this case is a pain in the ass to build. I would have thrown it away or returned it if my girlfriend didn't help me build it._ 

![pi-cluster](https://gitlab.com/joshsisto/blog/-/raw/101cdc45fd46ddebdae766953656dfea4fa9acfc/content/images/pi-cluster.jpg)

To get started I installed ubuntu 20.04.1 64bit OS. I named the systems k3-master, k3node1, k3node2, k3node3. You need to add `cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory` to `/boot/firmware/cmdline.txt` on each of the raspberry pi's to enable container features. Then you need to run the following command on the master pi.

> curl -sfL https://get.k3s.io | sh -

You can run the following command to verify k3s started correctly

> sudo systemctl status k3s

To view the master node run

> sudo k3s kubectl get nodes

While you are still connected to the k3-master run the following command to get the server token you will later use to connect the worker nodes. 

> sudo cat /var/lib/rancher/k3s/server/node-token

Now connect to one of your worker nodes and run the following command to connect the worker node to the master. You will need to replace 'myserver' with the IP of your master node. You also need to replace 'XXX' in K3S_TOKEN to the token that you got from the master node.

> sudo curl -sfL https://get.k3s.io | K3S_URL=”https://myserver:6443” K3S_TOKEN=XXX sh -

After you connect your node you can check wit the following command.

> sudo k3s kubectl get nodes

![kubectl](https://gitlab.com/joshsisto/blog/-/raw/101cdc45fd46ddebdae766953656dfea4fa9acfc/content/images/kubectl-get-nodes.png)

The next step is to configure the dashboard. I ended up using [this guide](https://blog.tekspace.io/deploying-kubernetes-dashboard-in-k3s-cluster/) since I was having trouble using [k3s official guide.](https://rancher.com/docs/k3s/latest/en/installation/kube-dashboard/)

I think the next steps are to start deploying containers!
