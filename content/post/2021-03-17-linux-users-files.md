---
title: Linux Users and Files
# subtitle: Making a Gallery
date: 2021-03-17
tags: ["linux"]
---

Some of this was inspired by [this post](https://carltheperson.com/posts/10-things-linux). First let's talk navigating linux. I'm assuming you are familiar with the `ls` command. Let's look at it a little further by exploring it's options and setting and alias. It's common for `ll` to be aliased as `ls -lh` which is more useful than the ls command alone. I typically like to add `-a` as well to show files preceded with a dot (.). Which essentially means show hidden files. The alias for that would look like `alias ll="ls -hal"`. In addition to the ls command you can use `tree` to show more directory information. Here is a [good article](https://www.thegeekdiary.com/understanding-basic-file-permissions-and-ownership-in-linux/) on understanding permissions.

To add the user steve and set password use the commands below.

`sudo useradd steve`

`sudo passwd steve`

This will create a new user according to `/etc/default/useradd` file.

Entries are added to `/etc/passwd`, `/etc/shadow`, `/etc/group` and `/etc/gshadow`.

It is common for a home directory to be created when adding a new user. For the sake of exercise I want to create the directory and set the permissions myself. Let's start by switching to the new user `su steve`. This will change us to the newly created user. Now try to create `/home/steve`. You will be unable to do so as the `steve` user. Instead you will have to use sudo. This will set the user and group owner as `root`. After creating the directory run the commands below to change the owner and group.

`sudo chown steve /home/steve/`

`sudo chown :users /home/steve`

Now let's download some files to play around with to continue learning linux. Use the commands below to download the [Linux Pocket Guide](https://linuxpocketguide.com) and unzip it.

`wget http://linuxpocketguide.com/LPG-stuff.tar.gz`

I played around with using different file names to see if it would change the digital hash. I tested this using the `sha1sum` command after downloading it on different machines with different names. Each returned the same signature db2ed9e750930beb4ed0850f143cdcb5b39312c4

`sha1sum LPG-stuff.tar.gz`

Now use the command below to unzip and extract the contents. Hence the extension .tar (tarball) .gz (gzip)

`tar -xf LPG-stuff.tar.gz`

Copy the the contents to other directories to really familiarize yourself with the `cp`, `scp`, and `rsync`. Know how and when to use each one. I recommend using rsync to copy files to and from a remote server. Understand the difference between a hardlink and a softlink. Hardlink and softlink files that you just downloaded. Use the `ls -i` command to show the inode number and see how it is the same for softlink data. You can also use `readlink -f` to show the root file of a linked file. 

Need to include find

`find / -name "passwd"` find files containing passwd

`find / -name "*.txt"` find files with extension .txt



