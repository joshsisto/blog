---
title: Cherrytree
# subtitle: Making a Gallery
date: 2022-01-17
tags: ["notes", "hacking"]
---

I've been working through HACKTHEBOX machines as I study up for the Pentest+ exam. One thing I noticed while doing a walkthroughs and in the pentester community in general was the usage of [cherrytree](https://www.giuspen.com/cherrytree/) for notetaking. Currently as a mac user I have been using Apple notes. Apple notes is a decent note taking program with the exception that it is restricted to the Apple ecosystem. Lately I have been doing a lot of pentesting on a separate laptop with Kali linux installed. I also have a virtual Kali instance but I find having real hardware to work better in my situation. Installing Cherrytree on Kali is a breeze, simply run `sudo apt install cherrytree`. On my mac things were not quite as easy. It was recommended to use `brew install cherrytree`. When using brew however I never got a GUI window to open when running cherrytree. So I uninstalled and ended up building from source. To keep notes synced between machines I am using [syncthing](https://syncthing.net/). The only issue with this solution is when updating notes on one machine I need to reopen the note on any other machines to load the updated notes.

