---
title: One Year Off of Social Media
# subtitle: Making a Gallery
date: 2022-02-09
tags: ["social media", "privacy", "security"]
---

It's been [1 year since I deactivated or deleted](https://sisto.blog/post/2021-02-09-no-social-media/) all of my social media accounts. I guess not all social media sites because I did reactivate my LinkedIn profile. After 1 year of being off of social media I am definitely not going back. I enjoy better quality of life because I have more free time to myself to pursue things that make me happy, instead of doing the endless scroll trance for 15 minutes at a time and getting little to no value. Besides getting my time back I also get my full mental focus back. I no longer think through the lens of "what will get the most likes?" or "should I take a photo of this moment to post online?" or think about checking on likes of posts or photos for social validation and to receive positive or negative reinforcement based on likes and others engagement. Since getting rid of social media I live more in the moment.


Getting rid of social media is part of a bigger plan to remove myself from [Big Tech](https://en.wikipedia.org/wiki/Big_Tech) as much as possible (without being too inconvenient). I am working on trying to take my data back and not hand it over to big tech. I take issue with the big tech companies tracking every measurable metric of personal data that they can so they can mostly sell you adds all in the pursuit of profit. Big tech also blocks competitors or buys them up before they can become a real challenger, this stifles innovation. They also use their market power to an unfair advantage that again hurts consumers. I believe that Apple is the lesser of the evils and that is the route I have chosen with an iPhone and macbook. I only use Microsoft Windows on my work issued laptop. As for Amazon I am prime member, I try to use the service sparingly and shop locally when possible. Maybe this year will be the year I cut the prime cord!


Recently I removed myself from google as well. At first I switched from Chrome to Firefox and I changed my search engine to DuckDuckGo. But I still had not switched over from gmail. So many accounts have been setup with that account over the years! I logged in to my password manager and went through the list and switched everything over from joshsisto@gmail.com to josh@joshsisto.com. I also recently switched my web browser to brave which has it's own search engine. I can also easily search DuckDuckGo or google quickly using :d or :g respectively to switch search engines on the fly. When using google search I make sure to stay signed out and use a VPN to help avoid tracking. I also removed all Google apps from my cell phone. 


I've cut out all Meta services for over a year with no plans to return to any of their services. I've now removed google but I'm sure I will still occassionaly use their search since it is still superior in some instances. I'm still an Amazon Prime member but I am considering cutting that subscription. Good timing since they are raising prices this year. I only use Microsoft products for work, except I do pay for Microsoft Office for personal use. I have no immediate plans to replace Apple. Maybe when I replace my macbook I'll replace it with something that runs linux. And my next phone? Maybe I'll replace it with a dumb flip phone or a pine phone.
