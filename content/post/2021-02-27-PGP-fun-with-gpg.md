---
title: PGP Fun With GPG
# subtitle: Making a Gallery
date: 2021-02-27
tags: ["security", "PGP", "encryption"]
---

```text
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

I've been having a lot of fun playing around with public key cryptography.
With all the ways information can be intercepted and tampered it is cool
knowing you can verify the integrity of your data. This text was generated
using the "--clearsign" option and can be verified with my public pgp key
https://josh.sisto.solutions/pgp.txt
Download my public key and import it into your keyring. You will also need
to copy this message and save it as a text file. For this example I will
call it pgp-blog-post.txt
To verify the message use the command below
gpg --verify pgp-blog-post.txt
This text output should show the message as a "Good signature".
If you don't see good signature then I didn't sign these words!
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEKUzffI0Yq91big2qjNuJ+DC8YOoFAmA7KSAACgkQjNuJ+DC8
YOrP9w//c6saTj7lbILUTBzSh+ecyDaqfcmxTR1wjvDn26ylqjHwflmF+0YIpDMz
zG6CmOdIwsiAri5ppHU1bP7zOoq8xmd4Nu75pyM79SWfAPX/9NDGmTKReiDBJoJy
MoiSkbjU30hzEs3dAnC2usoL1Y/hGyUafHV2j1CV9EWde4i4ma8r5pr6PYCncufV
Sad21Ig3Eu2fj9WgomJz+K+eqTd1cwe25EZYGtOeka2S4wh5qzi4ytMXHugg+4fR
1bVI1DL2PxdeBRC3lmeaROZBIrfmMdTs8NQfTsfUQRSR1phuosa4EdC5Zbd3DkeD
82SmDzYVm3NmRVLHnChm0VbhssNZM36kha0EKK/3iXzYtoyYamUYnot9F4N8JJjF
dy55HLsHut4L02iELIMYwXNWuFlbb6PiqrKPguiLsD4HDWrTWH8ygDvBd31UgA9h
sil9TwxOw07lBx91rLrmCyBHS/3rl6GUJbjdIsZCEUi/75VDGARt7JNcprV1r7vS
l1Fcg+M4piiGwKU9axR5K400glnAmb9ME4/X1VDpKqOBJwOKB9Iu0qj1c8pR2zeG
pdlohvYJQQ7NWIpqhqOxv0cHk3zhUbq3/bCvyZIGja4Bk2j6C7MJn49j1vwy7lb1
OuzHA4Hf3uBGZq6dPOQCo9ChM1X+0Te+ClnTyukBALrRAN/CP1o=
=VJyG
-----END PGP SIGNATURE-----
```

You can verify the message above here https://dark.fail/pgp

Insert my message above and my [public key](https://josh.sisto.solutions/pgp.txt)
