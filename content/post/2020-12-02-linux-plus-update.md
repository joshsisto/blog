---
title: Linux+ Update
# subtitle: Making a Gallery
date: 2020-12-02
tags: ["comptia", "linux"]
---

I've been slacking but I'm still working towards my CompTIA Linux+ certification. I bought an external SSD for my macbook because I was running into space issues editing videos and hosting VM's. I moved all my video footage and VM's to a 2TB Samsung T7. I also upgraded my Mikrotik RB951G to a newer hEX S model. I configured the second firewall to create a lab network where I can play around a little more freely. I recently configured my own "What's My IP" style app, you can check it out [here](https://josh.sisto.solutions/get-info/). If I stay focused hopefully I will have this certification completed before the end of the year.
