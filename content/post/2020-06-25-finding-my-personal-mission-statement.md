---
title: Finding My Personal Mission Statement
# subtitle: Making a Gallery
date: 2020-06-25
tags: ["self-help", "reading"]
---

I'm currently attempting to read and apply [The 7 Habits of Highly Effective People](https://www.franklincovey.com/the-7-habits.html) by Stephen Covey. This is probably my third or fourth attempt, as it takes a lot of time and work to apply the lessons. Following the 7 Habits I'm attempting to write my personal mission statement. A personal mission statement is like a personal constitution, a set of rules and guidelines to live by. Before I start any endeavor I think to myself, "Does this align with my personal mission statement?". This is a work in progress and will never be fully finished. That's why I've chose to use git to host my personal mission statement so you can view the history of it and see how it has changed over time. Well [here](https://gitlab.com/joshsisto/personal-mission-statement/-/blob/master/README.md) is the placeholder for my personal mission statement which I'm going to start working on now!