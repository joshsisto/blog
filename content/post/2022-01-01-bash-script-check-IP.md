---
title: Bash Script to Check Public IP Address
# subtitle: Making a Gallery
date: 2022-01-01
tags: ["linux", "bash"]
---

Created a simple bash script that checks if I am using my home public IP address or if I am using a VPN. I created this because I have been doing a lot of pentesting lately and I want to ensure that I am using a VPN before performing a pentest. This script performs an `nslookup` command against my Mikrotik provided DDNS service that is built into my firewall to get my home IP address. The script then executes `curl --silent ifconfig.me` to get the current public IP address of the machine running the script. The last part of the script compares the IP addresses and tells you if you are using a VPN or not. I could further expand on this to take some other action depending on if I am using my home IP address or not. 

https://gitlab.com/joshsisto/linux_scripts/-/blob/7d2b19fd44cf0bc6d06e61212699f321845d7d0f/check-vpn.sh
