---
title: RetroPie
# subtitle: Making a Gallery
date: 2021-01-17
tags: ["linux", "pi"]
---

I obviously love raspberry pi's and I have been wanting to share my [RetroPie image](https://josh.sisto.solutions/retropie/) for some time. This image contains everything you need to get going. Simply unzip the file and then flash it to a microSD card (32gb or bigger) and you are good to go. Connect your controllers and begin having fun. This image is designed for raspberry pi 4. I can find my raspberry pi 3 image if you send me an email. This image is huge because I just did a dd of the microSD card. I know that I could shrink it down but I haven't gotten around to it.

![LEGO NES and SNES](https://gitlab.com/joshsisto/blog/-/raw/master/content/images/nes_snes.jpg)
LEGO NES and SNES

![LEGO genesis](https://gitlab.com/joshsisto/blog/-/raw/master/content/images/genesis.jpg)
LEGO SEGA Genesis

This was also a fun opportunity for me to figure out how to host a 24GB file. I looked around at some services and then thought "why not self host". My raspberry pi that runs apache only has a 32GB SD card so space is limited. So I put the image on a USB drive and then mounted the drive in /var/www/html/retropie

An issue I face often with flashing USB and microSD cards is having trouble getting them back to their factory setting as a usable storage device. On my mac the easiest way to revert a drive is use the following commands

Identify the usb drive

> diskutil list

zero the drive. Replace X with the corresponding number for the USB drive

> diskutil zeroDisk /dev/diskX

After this initializes you can cancel. You don't need to wait for the zerdisk to complete. Then open DiskUtility and format the drive to the factory setting.

RetroPie Image: https://josh.sisto.solutions/retropie/
