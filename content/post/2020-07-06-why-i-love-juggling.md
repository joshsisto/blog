---
title: Why I Love Juggling
# subtitle: Making a Gallery
date: 2020-07-06
tags: ["learning", "juggling"]
---

Someone asked me last month why I love juggling so much. It actually took me a second to think about what I had just been asked, I then answered "because I love learning". Learning to juggle follows the same process as all other learning. I believe that all learning is connected.

When learning something new I focus on one concept and then break it down to the smallest components. Take the [3 ball cascade](http://www.libraryofjuggling.com/Tricks/3balltricks/Cascade.html) for example, this is one of the first juggling moves people attempt to learn. If you follow the instructions in the link it tells you to start with just one ball. This seems trivial but most people find difficulty throwing correctly with their non-dominant hand. If you break it down even further you will notce the ball is caught on the outside and thrown from the inside. The opposite would be the [reverse cascade](https://www.libraryofjuggling.com/Tricks/3balltricks/ReverseCascade.html), a much more difficult trick that should be avoided when attempting to learn 3 ball cascade. After you get the hang of 1 ball back and forth you can move on to 2 balls. When juggling 2 balls you want to throw to each hand similar to how you were throwing with one ball. When juggling 2 balls you are learning the timing of 3 ball cascade. I notice beginner learners have a tendency to want to throw the balls quickly out of their hand similar to a flash (a somewhat difficult trick). Learn to play with the timing, try to wait until the last second to throw the second ball. After you get the hang of 2 you are ready to move on to 3 balls. 

You can follow the links that I've provided for detailed instructions on how to learn many tricks. The point I'm trying to make is that in my experience it helps to practice with purpose, and to break things in to the smallest movements possible when learning. I also believe that it helps to have concepts in mind when learning to juggle. Some concepts include, timing, sequential movement, and mastering the throw right before catching with the same hand. 

I believe that a lot of the same ideas put here can be applied to other forms of learning. One of the most important factors to learning is spending the time doing it. You can be efficient with your time, but there is no replacement for practicing frequently and putting in the reps. Malcolm Gladwell popularised the 10,000 hour rule in his book Outliers where he basically says it takes 10,000 hours to become highly proficient in any skill. I believe there is a lot of power in deliberate practice.

I've done a lot of reading on the subject of happiness and one of the things that I've come to believe brings people happiness is building skills. Which brings me to my last point on why I love juggling. I enjoy the process of skill building. It feels great to be good at something and even better to be useful. Now go out and practice your passion or hobby!
