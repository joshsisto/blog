---
title: Switching My Domain To CloudFlare
# subtitle: Making a Gallery
date: 2022-01-27
tags: ["networking", "hosting", "security"]
---

I have been using cloudflare for my [sisto.solutions](https://sisto.solutions) domain for years and it has worked great. I use it as a proxy when hosting services from my home servers. This allows me to host websites and services from my home servers without exposing my public IP address. I recently moved my joshsisto.com domain over to cloudflare by switching the Name Servers from Google to CloudFlare. CloudFlare copied most of the records automatically when I switched the nameservers over but I still had to do a little work. My email continued to work but I had to fix [DMARC and DKIM](https://mxtoolbox.com/deliverability/f53b4c63-a3d0-4138-9933-7b6eb35e39e7). I used [MXTOOLBOX](https://mxtoolbox.com/) to check my records by sending an email to ping@mxtoolbox.com. This sent me a report with what needed to be fixed and I made the necessary changes to my domain and now have a [clean report](https://mxtoolbox.com/deliverability/fa1b44d3-e870-4a9a-9fd0-82038757418d). 

The original reason I moved my joshsisto.com domain over to cloudflare was to proxy my home IP so I can host my site on my home servers without exposing my home IP address. I chose this route because it is free instead of paying for a hosting service, and it is private and secure.

In CloudFlare I created a A Record and pointed it to my home address with the proxy option selected. Now I needed to update my [swag](https://docs.linuxserver.io/general/swag) server. I wrote about my setup in a [former blog post](https://sisto.blog/post/2020-05-29-website_hosting_from_home/). Basically it is a NGINX reverse proxy that uses Let's Encrypt for certs. I used the EXTRA_DOMAINS option in my docker compose file. Next I used [this guide](https://tech-com.co.uk/swag/) to configure my swag server.

I copied /config/nginx/site-confs/default and created default2 /config/nginx/site-confs/default2

In default2 file I put the following

```bash
server {
listen 80;
listen [::]:80;
server_name joshsisto.com;
return 301 https://$host$request_uri;
}

server {
listen 443 ssl http2;
listen [::]:443 ssl http2;

root /config/www2;
server_name joshsisto.com;	

# all ssl related config moved to ssl.conf
include /config/nginx/ssl.conf;
    
client_max_body_size 0;

location / {
    include /config/nginx/proxy.conf;
#    proxy_pass http://10.1.1.241:8080;
  }
}
```

I also created /config/www2/ directory and added /config/www2/index.html

When making changes use ```docker container restart swag``` to restart container and see changes

This did not start working until I changed CloudFlare SSL/TLS to Full Encrypt