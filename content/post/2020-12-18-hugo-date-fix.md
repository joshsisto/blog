---
title: Hugo Date Fix
# subtitle: Making a Gallery
date: 2020-12-18
tags: ["blogging", "debug"]
---

I was about to create a blog post when I noticed that the dates weren't working on my posts. This was concerning because having a date on a blog post is important to me. I did a little googling and debugging and didn't find an easy answer right away. So I started the hugo server locally. I have everything hosted via GitLab Pages so I never even bothered testing or running the server locally. To start the server enter `hugo server` at the command line. Once I did this I immediately got the following error displayed several times  

> "WARN 2020/12/18 18:16:56 Failed to get translated string for language "en" and ID "postedOnDate": template: :1:13: executing "" at <.Count>: can't evaluate field Count in type string".

I googled the error and found a [discussion](https://discourse.gohugo.io/t/hugo-v0-78-2-regression-executing-at-count-cant-evaluate-field-count-in-type-string/29411) with a fix on the error. The solution was *Replace {{ .Count }} by {{ . }}. The problem seems to originate from go-i18n, see here: [https://github.com/gohugoio/hugo/issues/7822](https://github.com/gohugoio/hugo/issues/7822)*. I entered `grep -r "{{ .Count }}" .` which returned

> ./themes/beautifulhugo/i18n/en.yaml:  translation: "Posted on {{ .Count }}"

I replaced {{ .Count }} with {{ . }} as suggested and fixed the date.
