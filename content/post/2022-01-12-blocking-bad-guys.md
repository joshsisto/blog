---
title: Blocking Bad Guys
# subtitle: Making a Gallery
date: 2022-01-12
tags: ["security", "networking"]
---

In my last [blog post](https://sisto.blog/post/2022-01-05-mikrotik-firewall-security/) I went over some firewall security rules that I have setup. Since then I've been thinking about how I can continue to increase securtity of my home network. What I came up with was to block traffic from any IP that visits my common ports. I created a filter rule that looks for people checking ports 21,22,23,25,53,110,111,135,139,143,445,993,995,1723,3389,5900 over the WAN interface. The rule then adds the user to a bad-guys list for a week, all traffic is dropped from bad-guys list. These rules have been in place for a little under a week and there are already over 1300 [IP's that are being blocked](https://gitlab.com/joshsisto/blog/-/raw/master/content/assets/bad-guys.txt). Besides blocking common ports I still have port scanning blocked as well. Port scanning doesn't get flagged often since most people are blocked by scanning the common ports first. These rules have worked well at blocking potential bad actors and at the same time still self hosting websites without any downtime. This setup has also complimented my port knocking rules beautifully. In the future I want to make a video showing off some of my Mikrotik firewall rules. 

[Bad guys list](https://gitlab.com/joshsisto/blog/-/raw/master/content/assets/bad-guys.txt)
