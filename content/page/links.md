---
title: Links
# subtitle: Links I Find Useful
comments: false
---

### Security & Privacy

[Vulnerability Check](https://secalerts.co/security-audit)
[Google Alternatives](https://restoreprivacy.com/google-alternatives/)
[Firefox Privacy](https://restoreprivacy.com/firefox-privacy/)

### Programming

[Book Of Secret Knowledge](https://github.com/trimstray/the-book-of-secret-knowledge)
[Free Programming Books](https://github.com/EbookFoundation/free-programming-books/blob/master/free-courses-en.md)
[Awesome List Of Awesome Lists](https://github.com/bayandin/awesome-awesomeness)
[Another Awesome List Of Awesome Lists](https://github.com/sindresorhus/awesome)

### Web Dev

[Front End Checklist](https://frontendchecklist.io/)
[Design Tools](https://flawlessapp.io/designtools)
[Front-End Developer (2019 Edition)](https://geekflare.com/become-a-front-end-developer/)
[Front-End Developer Handbook 2019](https://frontendmasters.com/books/front-end-handbook/2019/)

### Social Media

[Facebook Activity](https://www.facebook.com/off-facebook-activity)
[Delete FB](https://github.com/weskerfoot/DeleteFB)

### Blogs

[https://sivers.org/](https://sivers.org/)
[https://www.troyhunt.com/](https://www.troyhunt.com/)
[https://maxmasnick.com/kb/](https://maxmasnick.com/kb/)
[https://chrisalbon.com/](https://chrisalbon.com/)
[https://www.jvt.me/](https://www.jvt.me/)
[https://ryannjohnson.com/](https://ryannjohnson.com/)
[https://schollz.com/blog/](https://schollz.com/blog/)
[https://www.perell.com/start-here](https://www.perell.com/start-here)
[https://nickjanetakis.com/](https://nickjanetakis.com/)
[https://www.superhighway98.com/](https://www.superhighway98.com/)
[https://beepb00p.xyz/site.html](https://beepb00p.xyz/site.html)
[https://nikitavoloboev.xyz/](https://nikitavoloboev.xyz/)
[https://www.gwern.net/](https://www.gwern.net/)
[https://philosopher.life](https://philosopher.life)
[https://jjbeshara.com/blog/](https://jjbeshara.com/blog/)
[https://modelpredict.com/python-dependency-management-tools](https://modelpredict.com/python-dependency-management-tools)
[https://www.grahamcluley.com/](https://www.grahamcluley.com/)
[https://danielmiessler.com/](https://danielmiessler.com/)
[https://krebsonsecurity.com/](https://krebsonsecurity.com/)
[https://thehackernews.com/](https://thehackernews.com/)
