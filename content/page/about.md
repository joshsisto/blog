---
title: About Me
# subtitle: Why you'd want to hang out with me
comments: false
---

### About

![Yosemite ME](https://gitlab.com/joshsisto/blog/raw/5988f60b85dc895dd06763fe1608d5c0e93f93ba/content/images/yosemite.jpg)

Hi 👋 I'm Josh Sisto. I currently work as a Security Analyst for the State of California. I also work as CTO and volunteer for [Packs For Cold Backs](https://www.packsforcoldbacks.org/). When I'm not behind a computer you can find me riding my bicycle, camping, hiking, rock climbing, long distance running, practicing jiu-jitsu, or juggling.

### [2019 Photos](https://photos.app.goo.gl/3ikUR1MJPMgHSKe66)

"Dogs are not our whole life, but they make our lives whole." - [Roger Caras](https://www.goodreads.com/quotes/19168-dogs-are-not-our-whole-life-but-they-make-our)

![Jafar](https://gitlab.com/joshsisto/blog/raw/5988f60b85dc895dd06763fe1608d5c0e93f93ba/content/images/jafar.jpg)
Jafar 2019
