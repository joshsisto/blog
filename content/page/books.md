---
title: Books
# subtitle: Books that I have read
comments: false
---

### Currently Reading

Enlightenment Now by Steven Pinker

### Most Impactful Books (Kind of In Order of Importance)

The 7 Habits of Highly Effective People by Stephen Covey

Social Animal by David Brooks

48 Laws Of Power by Robert Greene

Born To Run by Christopher McDougall

The Alchemist by Paulo Coelho

Stumbling On Happiness by Daniel Gilbert

Happiness Hypothesis by Jonathan Haidt

The Art of Asking by Amanda Palmer

The War of Art Steven Pressfield

### Past Books

Outliers

The Tipping point

The Bhagavad Gita

The Better Angels of Our Nature

Influence

Tribe

The Inevitable

#### To-Do

Write articles about the books I've read starting with the most impactful
